import 'package:flutter/material.dart';

void main() => runApp(HelloFlutterApp());

class HelloFlutterApp extends StatefulWidget {
  @override
  _MyStatefuWidgetState createState() => _MyStatefuWidgetState();
}
String englishGreeting = "Hello Flutter";
String spanishGreeting = "Hola Flutter";
String janpaneseGreeting = "Konichiwa Flutter";
String koreaGreeting = "Annyeonghaseyo Flutter";
String chinsesGreeting = "Nihao Flutter";
String thaiGreeting = "Sawatdee Flutter";

class _MyStatefuWidgetState extends State<HelloFlutterApp>{
  String displayText = englishGreeting;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,

      home: Scaffold(
        appBar: AppBar(
          title: Text("Hello GunGun"),
          leading: Icon(Icons.home),
          actions: <Widget>[
          IconButton(
              onPressed: () {
                setState(() {
                  displayText = displayText == englishGreeting?
                  spanishGreeting : englishGreeting;
                });
              },
              icon: Icon(Icons.refresh)),
            IconButton(
                onPressed: () {
                  setState(() {
                    displayText = displayText == koreaGreeting?
                    janpaneseGreeting : koreaGreeting;
                  });
                },
                icon: Icon(Icons.filter_drama)),
            IconButton(
                onPressed: () {
                  setState(() {
                    displayText = displayText == chinsesGreeting?
                    janpaneseGreeting : chinsesGreeting;
                  });
                },
                icon: Icon(Icons.commute)),
            IconButton(
                onPressed: () {
                  setState(() {
                    displayText = displayText == thaiGreeting?
                    englishGreeting : thaiGreeting;
                  });
                },
                icon: Icon(Icons.explore))
        ],
        ),

        body: Center(
          child: Text(
            displayText,
            style: TextStyle(fontSize: 24)
          ),
        ),
      ),
    );
  }
}
















// class HelloFlutterApp extends StatelessWidget {
// @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text("Hello GunGun"),
//           leading: Icon(Icons.home),
//           actions: <Widget>[
//             IconButton(
//                 onPressed: () {},
//                 icon: Icon(Icons.refresh))
//           ],
//         ),
//         body: Center(
//           child: Text(
//             "Hello Flutter" ,
//             style: TextStyle(fontSize: 24)
//           ),
//         ),
//       ),
//     );
//   }
// }